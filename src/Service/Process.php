<?php

use Symfony\Component\Process\Exception\ProcessFailedException;

class Process
{

    /**
     * @param string $command
     * @return string
     */
    public function runCommand(string $command): string
    {

        $process = \Symfony\Component\Process\Process::fromShellCommandline($command);

        $process->run();
        if (!$process->isSuccessful()) {

            throw new ProcessFailedException($process);
        }

        return $process->getOutput();

    }


}